minetest.override_item("mcl_farming:beetroot_soup", { stack_max = 64 })

minetest.register_craft({
	output = "mcl_mobitems:saddle",
	recipe = {
		{"mcl_mobitems:leather", "mcl_core:iron_ingot", "mcl_mobitems:leather"},
		{"mcl_mobitems:leather", "mcl_mobitems:leather", "mcl_mobitems:leather"},
		{"mcl_mobitems:leather", "mcl_core:iron_ingot", "mcl_mobitems:leather"}}})

minetest.register_chatcommand("bed", {
    privs = {
        interact = true,
    },
    func = function(name, param)
		local player = minetest.get_player_by_name(name)
		if not player then
			return false, "No player" .. name
		end
		local bed_pos = mcl_spawn.get_bed_spawn_pos(player)
		player:set_pos(bed_pos)
        return true, "Teleporting " .. name .. " to respawn location " .. tostring(bed_pos)
    end,
})